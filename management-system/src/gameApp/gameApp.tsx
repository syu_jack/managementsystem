import Button from "@mui/material/Button";

export function GameApp() {
  return (
    <div className="gameApp">
      <Button variant="contained" href="gbf">
        グラブル
      </Button>
      <div />
      <Button variant="contained" href="pokemonGo">
        ポケモンGO
      </Button>
    </div>
  );
}

export default GameApp;
