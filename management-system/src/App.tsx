import {
  AppBar,
  Box,
  Button,
  Drawer,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import React, { useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import GameApp from "./gameApp/gameApp";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <HeaderMenu />
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Top />} />
            <Route path="/GameApp" element={<GameApp />} />
          </Routes>
        </BrowserRouter>
      </div>
    );
  }
}

function Top() {
  return <div>トップページ</div>;
}

function HeaderMenu() {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [menuOpen, setMenuOpen] = useState(false);

  // ヘッダのメニューアイコン押下時
  const onClickMenuButton = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
    setMenuOpen(true);
  };

  // ヘッダのメニューを閉じる
  const closeMenu = () => {
    setAnchorEl(null);
    setMenuOpen(false);
  };

  // メニュー内の「グラブル」押下時
  const onClickGbf = () => {
    document.location.href = "http://localhost:3000/GameApp";
  };

  // [トップへ戻る]ボタン押下時
  const toTopPage = () => {
    document.location.href = "http://localhost:3000/";
  };

  return (
    <AppBar position="static" color="primary">
      <Toolbar>
        {/* メニューアイコン */}
        <IconButton
          size="large"
          edge="start"
          color="inherit"
          sx={{ mr: 2 }}
          onClick={onClickMenuButton}
        >
          <MenuIcon />
        </IconButton>

        {/* メニューアイテム */}
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={menuOpen}
          onClose={closeMenu}
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
        >
          <MenuItem onClick={onClickGbf}>GameApp</MenuItem>
          <MenuItem onClick={() => {}}>My account</MenuItem>
          <MenuItem onClick={() => {}}>Logout</MenuItem>
        </Menu>

        {/* 画面タイトル */}
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          メニュー
        </Typography>

        {/* トップへ戻る */}
        <Button color="inherit" onClick={toTopPage}>
          トップへ戻る
        </Button>
      </Toolbar>
    </AppBar>
  );
}

// TBD：
function leftMenu() {
  return (
    <div>
      {/* <Button onClick={toggleDrawer(anchor, true)}>{anchor}</Button> */}
      <Drawer
        anchor={"left"}
        open={true}
        // onClose={toggleDrawer(anchor, false)}
      >
        {"bbb"}
      </Drawer>
    </div>
  );
}

export default App;
